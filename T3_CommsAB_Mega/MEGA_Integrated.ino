#include <LU.h>
#include <Motion.h>

/*
 * Robot
 * 
 * PINS
 * ----
 * GND          ->  BT GND
 * 5V Power     ->  BT VCC
 * (16) TX2     ->  BT RX
 * (17) RX2     ->  BT TX
 * (36) Digital ->  BT State
 * 
 */

// Comms A Setup
unsigned char inv[6] = {4,4,4,4,4,4};
Motion *m;
LU *l;

void setup() {
    Serial.begin(9600);
    Serial.println("R: " + String(l->getRedNeed()) + ", B: " + String(l->getBlueNeed()) + ", G: " + String(l->getGreenNeed()));
    if (!establishConnectionToServer()) {Serial.println("Wc:F"); Serial.flush(); setup();}
    else {Serial.println("Wc:P");}
    delay(1000);
    Serial.println("R: " + String(l->getRedNeed()) + ", B: " + String(l->getBlueNeed()) + ", G: " + String(l->getGreenNeed()));
    delay(1000);
    Serial.println("R: " + String(l->getRedNeed()) + ", B: " + String(l->getBlueNeed()) + ", G: " + String(l->getGreenNeed()));
}

void loop() {
    //Serial.println("R: " + String(l->getRedNeed()) + ", B: " + String(l->getBlueNeed()) + ", G: " + String(l->getGreenNeed()));
    // call sendDone() when task complete
    // uncomment storeCanInfo() code once relevant libraries are included

    if(l->getTotal() > 0){
        //RED
        if (l->getRedNeed() > 0) {
            if (inv[0] > 0) {
                m->setLocation(RED1);
                m->setStatus(MOTION);
                //input motion team
                //assume no errors
                if (m->getStatus() == MHALT) {
                    l->setStatus(PICK);
                    //input loading team
                    //return the number picked up as red
                    // x is an amount that the other team collected
                    l->setRedNeed(l->getRedNeed() - l->getCount());//??
                    inv[0] -= l->getCount();
                    l->setCount(0);
                    l->setStatus(HALT);
                }
            }
            if (l->getRedNeed() > 0 && inv[1] > 0) {
                m->setLocation(RED2);
                m->setStatus(MOTION);
                //input motion team

                if (m->getStatus() == MHALT) {
                    l->setStatus(PICK);
                    //input loading team
                    //return the number picked up as red
                    // x is an amount that the other team collected
                    l->setRedNeed(l->getRedNeed() - l->getCount());//??
                    inv[1] -= l->getCount();
                    l->setCount(0);
                    l->setStatus(HALT);
                }
            }
        }

        //GREEN
        if (l->getGreenNeed() > 0) {
            if (inv[2] > 0) {
                m->setLocation(GREEN1);
                m->setStatus(MOTION);
                //input motion team
                //assume no errors
                if (m->getStatus() == MHALT) {
                    l->setStatus(PICK);
                    //input loading team
                    //return the number picked up as red
                    // x is an amount that the other team collected
                    l->setGreenNeed(l->getGreenNeed() - l->getCount());//??
                    inv[2] -= l->getCount();
                    l->setCount(0);
                    l->setStatus(HALT);
                }
            }
            if (l->getRedNeed() > 0 && inv[3] > 0) {
                m->setLocation(GREEN2);
                m->setStatus(MOTION);
                //input motion team

                if (m->getStatus() == MHALT) {
                    l->setStatus(PICK);
                    //input loading team
                    //return the number picked up as red
                    // x is an amount that the other team collected
                    l->setGreenNeed(l->getGreenNeed() - l->getCount());//??
                    inv[1] -= l->getCount();
                    l->setCount(0);
                    l->setStatus(HALT);
                }
            }
        }

        //BLUE
        if (l->getBlueNeed() > 0) {
            if (inv[4] > 0) {
                m->setLocation(BLUE1);
                m->setStatus(MOTION);
                //input motion team
                //assume no errors
                if (m->getStatus() == MHALT) {
                    l->setStatus(PICK);
                    //input loading team
                    //return the number picked up as red
                    // x is an amount that the other team collected
                    l->setBlueNeed(l->getBlueNeed() - l->getCount());//??
                    inv[4] -= l->getCount();
                    l->setCount(0);
                    l->setStatus(HALT);
                }
            }
            if (l->getBlueNeed() > 0 && inv[5] > 0) {
                m->setLocation(BLUE2);
                m->setStatus(MOTION);
                //input motion team

                if (m->getStatus() == MHALT) {
                    l->setStatus(PICK);
                    //input loading team
                    //return the number picked up as red
                    // x is an amount that the other team collected
                    l->setBlueNeed(l->getBlueNeed() - l->getCount());//??
                    inv[5] -= l->getCount();
                    l->setCount(0);
                    l->setStatus(HALT);
                }
            }
        }

        if (l->getTotal() == 0) {
            m->setLocation(END);
            m->setStatus(MOTION);
            //input motion team
            //assume no errors
            if (m->getStatus() == MHALT && m->getLocation() == END) {
                l->setStatus(DROP);
            }
        }
    }

    // Manual serial monitor input
    if (Serial.available() > 0) {
        Serial2.write(Serial.read());
    }
}
