String commonPass = "676767";
String serverAddress = "D43639DB06FF";
//String robotAddress = "D43639DB0B00";
unsigned char requestTest = 'T';
unsigned char replyTest   = 't';



void clearBuffer() {
  while (Serial2.available() > 0) {
    Serial2.read();
  }
}


bool establishConnectionToServer() {
  /* Sets up initial connection between to server, by testing for an exisiting connection,
     checking if that connection is with the valid device.

     If a connection does not exist the HM-10 module is configured using AT commands
     (assumes any exisiting configuration is incorrect).

     Robot is setup as slave.

     Returns true if connection is established, false otherwise.

  */
  Serial.println("Starting establishConnectionToServer");
  unsigned int statePin = 36;

  // Timeouts used in respective sections, all in ms
  const unsigned int currentConnectionTimeout = 1000;
  const unsigned int ATcommandsTimeout = 2000;

  // Used as a reference time with millis(); function for timeouts
  unsigned long refTime;

  // Sets up state pin
  pinMode(statePin, INPUT);

  // Set up baud rate on software serial ports
  Serial2.begin(9600);

  clearBuffer();
  Serial.println("Buffer Clear");
  // Checks for current connection
  refTime = millis();
  while (millis() - refTime < currentConnectionTimeout) {
    if (digitalRead(statePin) == LOW) {

      // No current connection, begin configuring module
      int numAtCommands = 8;
      String startAtCommands[numAtCommands] = {"AT+RENEW", "AT+RESET", "AT+ALLO1", "AT+AD1" + serverAddress, "AT+ROLE0", "AT+TYPE2", "AT+PASS" + commonPass, "AT+RESET"};

      // Sets up bluetooth module
      for (int i = 0; i < numAtCommands; i++) {
        Serial2.print(startAtCommands[i]);
        Serial2.flush();
        // Ensures that module is responding to AT commands, if not it may already be connected
        refTime = millis();
        while (Serial2.available() < startAtCommands[i].length()) {
          if (millis() - refTime > ATcommandsTimeout) {
            break;
          }
        }
        clearBuffer();
      }
      break;
    }
  }
  // Once a connection has been established, it checks we are connected to the right device
  return testConnection();
}


boolean testConnection() {
  /* Tests for a valid connection by ensuring the response for a test message
     is the predefined valid one for this system.

     Returns true if expected response is found, false otherwise.
  */
  Serial.println("testing conn");
  // Timeout between waiting for a single response, in ms
  Serial2.setTimeout(500);

  for (int i = 1; i <= 10 /* Num of times test message is sent */; i++) {

    Serial2.write(requestTest);
    // Ensures both sides can run testConnection at the same time.
    if (Serial2.peek() == requestTest) {
      clearBuffer();
      Serial2.write(replyTest);
      Serial.println("test char detected");
    }
    if (Serial2.find(replyTest)) {
      clearBuffer();
      Serial.println("test reply char found, test true");
      return true;
    }
  }
  clearBuffer();
  Serial.println("test fail");
  return false;
}


void serialEvent2() {
  /* Deals with actioning all messages, and is a function called when there is
     data in the serial buffer that needs to be dealt with.

     This function will call itself when avaliable > 0 and loop() finishes.

     This function should be called whenever we are waiting for a message or inbetween
     actions to ensure a response to incoming messages.

  */

  // Test message in serial buffer
  if (Serial2.peek() == requestTest) {
    Serial2.read();
    Serial2.write(replyTest);
    Serial2.flush();
  }

  // Reading in messages from buffer
  //--------------------------------

  // Timeout for waiting for full message, in ms
  Serial2.setTimeout(250);

  // Stores current message being read in
  String currentMessage;

  // Can message type -- 5 characters long
  if (Serial2.peek() == 'C') {
    if (Serial2.available() >= 5) {
      return;
    }
    currentMessage = Serial2.readStringUntil('>');
  } else {
    Serial2.read();
  }

  // Actioning messages
  //-------------------

  // Ensures that there is a message stored in currentMessage
  if (currentMessage.length() > 0) {

    // Can message
    if (currentMessage[0] == 'C') {
      if (currentMessage[4] == 'c' && isdigit(currentMessage[1]) && isdigit(currentMessage[2]) && isdigit(currentMessage[3])) {
        Serial.println("Wm:CP");
        storeCanInfo(currentMessage);
      } else {
        Serial.println("Wm:CF");
      }

      // Undefined message
    } else {
      Serial.println("Wm:U");
    }

    currentMessage = "";
  }
}


void storeCanInfo(String canMessage) {
  // Sets object values for can information
  Serial.println("Previous values:");
  Serial.println("R: " + String(l->getRedNeed()) + ", B: " + String(l->getBlueNeed()) + ", G: " + String(l->getGreenNeed()));
  l->setRedNeed(canMessage[1]);
  l->setGreenNeed(canMessage[2]);
  l->setBlueNeed(canMessage[3]);
  Serial.println("New values");
  Serial.println("R: " + String(l->getRedNeed()) + ", B: " + String(l->getBlueNeed()) + ", G: " + String(l->getGreenNeed()));
}


void sendDone() {
  String message = "D>";
  Serial2.print(message);
  Serial2.flush();
}
