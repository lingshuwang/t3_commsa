#include "Motion.h"

Motion::Motion() {
    location = START;
    currLocation = START;
    status = MHALT;
}

void Motion::setLocation(unsigned char location){
    this->location = location;
}

void Motion::setCurrLocation(unsigned char currLocation) {
    this->currLocation = currLocation;
}

void Motion::setStatus(unsigned char status) {
    this->status = status;
}


unsigned char Motion::getLocation(){
    return this->location;
}

unsigned char Motion::getCurrLocation(){
    return this->currLocation;
}

unsigned char Motion::getStatus(){
    return this->status;
}