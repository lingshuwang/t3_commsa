//#include <Sensor.h>
#include <Servo.h>
#include <LU.h>
#include <Motion.h>


/*
   Robot

   PINS
   ----
   GND          ->  BT GND
   5V Power     ->  BT VCC
   (16) TX2     ->  BT RX
   (17) RX2     ->  BT TX
   (36) Digital ->  BT State

*/

// Comms A Setup
unsigned char inv[6] = {4, 4, 4, 4, 4, 4};
Motion *m;
LU *l;


//Comms C Setup

void armExtend ();
void armRetract();
void armStop();
void beltStop();
void beltRun();
void unclamp();
void clamp();
int unlock();
int lock();
int lShapeGet(int num);
int rollerGet(int numOfCans);
int getCans(int numOfCans);
void unload();


//belt and clamp pin, speed and variable declarations
Servo clampServo; //servo that moves the clamp

int beltENA = 10; //belt PWM input
int beltIn1 = 6; //belt current directions + or -
int beltIn2 = 7;
const int motorForward = 70; //belt motor speed forward
const int motorStop = 0; //used by both linear and belt motors

int rollerBeltLinearENA = 9; //linear motor PWM input
int rollerBeltLinerIn1 = 13; //linear current directions + or -
int rollerBeltLinearIn2 = 14;
int linearSpeedForward = 20; //linear motor speed forward

//
int linearENA = 11; //linear motor PWM input
int linearIn1 = 8; //linear current directions + or -
int linearIn2 = 9;

//clamp declarations
int clampPin = 12;
int clampRest = 0; //clamp rest and engaged values in degrees
int clampEngage = 170;

//can counter
int countSwitch = 2; //limit switch to count the cans
int switchStateCount = 0;
int prevSwitchStateCount = 0;
int restStateCount = 0;

//linear Switches
int forwardLimit = 0;
int rearLimit = 1;
int switchStateForward = 0;
int previousSwitchStateForward = 0;
int switchStateBack = 0;
int previousSwitchStateBack = 0;

int switch1 = 41;
int switch2 = 42;
int switchState1 = 0;
int previousSwitchState1 = 0;
int switchState2 = 0;
int previousSwitchState2 = 0;

//L-shape Global Variables
//Motor
Servo lServo;

//L-shape Sensor
const float VCC = 4.98; // Measured voltage of Ardunio 5V line
const float R_DIV = 10000.0; // Measured resistance of 3.3k resistor
const float STRAIGHT_RESISTANCE = 78000.0; // resistance when straight
const float BEND_RESISTANCE = 110000.0; //
const int sensor = A3;
float threshold = 50;
int sensorState = 0; // variable to store the value read from the sensor pin

//Outtake global variables
int solenoidpin = 30;    //defines solenoid @pin 30 (FINAL) <-- Change as required
int timer = (countSwitch * 1000) + 5000; // Gets can count and opens the lock until for timer + 5 secs
boolean error = false;

//indentify arms
int armIdentifier = 32;
bool armState = NULL; //true -> l-shape, false -> roller

//End Comms C


////Comms D Setup
//unsigned int moveRobot();
//Sensor canSensor(1, 2, 3, 4, 5); // CHANGE PIN LOCATIONS
//Sensor lineSensor(6, 7, 8, 9, 10); // CHANGE PIN LOCATIONS
//Servo myservoLeft;
//Servo myservoRight;

void setup() {
  Serial.begin(9600);
  //    Serial.println("R: " + String(l->getRedNeed()) + ", B: " + String(l->getBlueNeed()) + ", G: " + String(l->getGreenNeed()));
  //    if (!establishConnectionToServer()) {Serial.println("Wc:F"); Serial.flush(); setup();}
  //    else {Serial.println("Wc:P");}
  //    delay(1000);
  //    Serial.println("R: " + String(l->getRedNeed()) + ", B: " + String(l->getBlueNeed()) + ", G: " + String(l->getGreenNeed()));
  //    delay(1000);
  //    Serial.println("R: " + String(l->getRedNeed()) + ", B: " + String(l->getBlueNeed()) + ", G: " + String(l->getGreenNeed()));

  //    l->setRedNeed(8);
  //    l->setGreenNeed(8);
  //    l->setBlueNeed(8);
  //    l->setTotal(l->getRedNeed() + l->getGreenNeed() + l->getBlueNeed());


  //Comms C Setup
  //L-shape setup
  pinMode(switch1, INPUT_PULLUP);
  pinMode(switch2, INPUT_PULLUP);
  lServo.attach(6, 1000, 2000);
  pinMode(sensor, INPUT);

  switchState1 = digitalRead(switch1);
  previousSwitchState1 = switchState1;

  switchState2 = digitalRead(switch2);
  previousSwitchState2 = switchState2;

  //forward limit, rear limit and can counter declarations
  pinMode(forwardLimit, INPUT_PULLUP);
  pinMode(rearLimit, INPUT_PULLUP);
  pinMode(countSwitch, INPUT_PULLUP);

  //linear motions pins and defaults
  pinMode(linearENA, OUTPUT);
  pinMode(linearIn1, OUTPUT);
  pinMode(linearIn2, OUTPUT);
  digitalWrite(linearIn1, HIGH);
  digitalWrite(linearIn2, LOW);

  //roller belt pins and defualts
  pinMode(beltENA, OUTPUT);
  pinMode(beltIn1, OUTPUT);
  pinMode(beltIn2, OUTPUT);
  digitalWrite(beltIn1, HIGH); //Set initial motor direction  ***assuming this is forward for now
  digitalWrite(beltIn2, LOW); //set initial motor direction

  //clamp attaching and default position
  clampServo.attach(clampPin);
  clampServo.write(clampRest); //defaults clamp to resting position

  //Outtake setup
  pinMode(solenoidpin, OUTPUT); //sets solenoid as Output

  switchStateCount = digitalRead(countSwitch);
  prevSwitchStateCount = switchStateCount;
  if (prevSwitchStateCount == HIGH) {
    restStateCount = LOW;
  } else {
    restStateCount = HIGH;
  }
  Serial.begin(9600);
}

//End Comms C
//    // Comms D Motion Setup - CHANGE PIN LOCATIONS
//    attachInterrupt(digitalPinToInterrupt(18), ISRleft, RISING);
//    attachInterrupt(digitalPinToInterrupt(19), ISRright, RISING);
//    pinMode(22,INPUT); // 18 and 22 come from left
//    pinMode(23,INPUT); // 19 and 23 come from right
//    myservoLeft.attach(4);  // attaches the servo on pin 4 to the servo object
//    myservoRight.attach(5);  // attaches the servo on pin 5 to the servo object


void loop() {
  //Serial.println("R: " + String(l->getRedNeed()) + ", B: " + String(l->getBlueNeed()) + ", G: " + String(l->getGreenNeed()));
  // call sendDone() when task complete
  // uncomment storeCanInfo() code once relevant libraries are included

  if (l->getTotal() > 0) {
    //RED
    if (l->getRedNeed() > 0) {
      if (inv[0] > 0) {
        m->setLocation(RED1);
        m->setStatus(MOTION);
        //input motion team
        // moveRobot();
        //assume no errors
        delay(30000);//because motion doesnt work....will never work
        m->setStatus(MHALT);
        if (m->getStatus() == MHALT) {
          l->setStatus(PICK);
          l->setCount(getCans(l->getRedNeed()));
          //input loading team
          //return the number picked up as red
          // x is an amount that the other team collected
          l->setRedNeed(l->getRedNeed() - l->getCount());//??
          inv[0] -= l->getCount();
          l->setCount(0);
          l->setStatus(HALT);
        }
      }
      if (l->getRedNeed() > 0 && inv[1] > 0) {
        m->setLocation(RED2);
        m->setStatus(MOTION);
        //input motion team
        // moveRobot();

        delay(30000);//because motion doesnt work....will never work
        m->setStatus(MHALT);
        if (m->getStatus() == MHALT) {
          l->setStatus(PICK);
          l->setCount(getCans(l->getRedNeed()));
          //input loading team
          //return the number picked up as red
          // x is an amount that the other team collected
          l->setRedNeed(l->getRedNeed() - l->getCount());//??
          inv[1] -= l->getCount();
          l->setCount(0);
          l->setStatus(HALT);
        }
      }
    }

    //GREEN
    if (l->getGreenNeed() > 0) {
      if (inv[2] > 0) {
        m->setLocation(GREEN1);
        m->setStatus(MOTION);
        //input motion team
        // moveRobot();
        //assume no errors

        delay(30000);//because motion doesnt work....will never work
        m->setStatus(MHALT);
        if (m->getStatus() == MHALT) {
          l->setStatus(PICK);
          l->setCount(getCans(l->getGreenNeed()));
          //input loading team
          //return the number picked up as red
          // x is an amount that the other team collected
          l->setGreenNeed(l->getGreenNeed() - l->getCount());//??
          inv[2] -= l->getCount();
          l->setCount(0);
          l->setStatus(HALT);
        }
      }
      if (l->getGreenNeed() > 0 && inv[3] > 0) {
        m->setLocation(GREEN2);
        m->setStatus(MOTION);
        // moveRobot();
        //input motion team
        delay(30000);//because motion doesnt work....will never work
        m->setStatus(MHALT);
        if (m->getStatus() == MHALT) {
          l->setStatus(PICK);
          l->setCount(getCans(l->getGreenNeed()));
          //input loading team
          //return the number picked up as red
          // x is an amount that the other team collected
          l->setGreenNeed(l->getGreenNeed() - l->getCount());//??
          inv[1] -= l->getCount();
          l->setCount(0);
          l->setStatus(HALT);
        }
      }
    }

    //BLUE
    if (l->getBlueNeed() > 0) {
      if (inv[4] > 0) {
        m->setLocation(BLUE1);
        m->setStatus(MOTION);
        // moveRobot();
        //input motion team
        //assume no errors
        delay(30000);//because motion doesnt work....will never work
        m->setStatus(MHALT);
        if (m->getStatus() == MHALT) {
          l->setStatus(PICK);
          l->setCount(getCans(l->getBlueNeed()));
          //input loading team
          //return the number picked up as red
          // x is an amount that the other team collected
          l->setBlueNeed(l->getBlueNeed() - l->getCount());//??
          inv[4] -= l->getCount();
          l->setCount(0);
          l->setStatus(HALT);
        }
      }
      if (l->getBlueNeed() > 0 && inv[5] > 0) {
        m->setLocation(BLUE2);
        m->setStatus(MOTION);
        // moveRobot();
        //input motion team
        delay(30000);//because motion doesnt work....will never work
        m->setStatus(MHALT);
        if (m->getStatus() == MHALT) {
          l->setStatus(PICK);
          l->setCount(getCans(l->getBlueNeed()));
          //input loading team
          //return the number picked up as red
          // x is an amount that the other team collected
          l->setBlueNeed(l->getBlueNeed() - l->getCount());//??
          inv[5] -= l->getCount();
          l->setCount(0);
          l->setStatus(HALT);
        }
      }
    }


    l->setTotal(l->getRedNeed() + l->getGreenNeed() + l->getBlueNeed());
    if (l->getTotal() == 0) {
      m->setLocation(END);
      m->setStatus(MOTION);
      //input motion team
      // moveRobot();
      //assume no errors

      delay(30000);//because motion doesnt work....will never work
      m->setStatus(MHALT);
      if (m->getStatus() == MHALT && m->getLocation() == END) {
        l->setStatus(DROP);
      }
    }
  }

  // Manual serial monitor input
  //    if (Serial.available() > 0) {
  //        Serial2.write(Serial.read());
  //    }
}

//unsigned int moveRobot() {
//
//    // Return control to Comms A if status MHALT
//    if(m->getStatus() == 0)
//      return 1;
//
//    // Return control to Comms A
//    if(m->getCurrLocation() == m->getLocation()) {
//
//      // Disable Sensor Reading
//      canSensor.setStatus(0);
//      // Stop Robot Motion
//      myservoLeft.write(90);
//      myservoRight.write(90);
//      // Set Status to MHALT
//      m->setStatus(MHALT);
//      // Return Successfully
//      return 0;
//
//    }
//
//    // Expected can stations will be measured and hard coded when the robot is tested
//    // int target = canLocations[targetLocation] - canLocations[currentLocation];
//
//    // Prevents Sensor from reading the same color again at a location
//    if(canSensor.getCanSensorData() == 3)
//      canSensor.setStatus(1); // Set true if no color detected
//
//    // Function is expected to perform like a loop using the void loop instead with a series of if statements
//    if(m->getCurrLocation() != m->getLocation()) {
//
//        if(canSensor.getStatus() == 1 && getCanSensorData() != 3) {
//
//            if(m->getCurrLocation() == 6)
//              m->setCurrLocation(0);
//            else
//              m->setCurrLocation(m->getCurrLocation()++);
//
//            canSensor.setStatus(0);
//
//        }
//
//        // Set targetLocation to start if instructions are to move backwards
//        if(m->getLocation() < m->getCurrLocation())
//            m->setLocation(START);
//
//        // TO BE COMPLETED - Implement PID Controller and tune values
//
//        int throttleLeft = (int) 90.0 - 0.9 * (target-countLeft);
//        int throttleRight = (int) 90.0 + 1.3 * (target-countRight);
//
//        // Adjust Robot to Black and White tape
//        int diff = canSensor.getLineSensorData();
//
//        if(diff > greyScaleConstant) {
//          myservoRight.write(90 + (10  * (throttleLeft / |throttleLeft|)));
//        } else {
//          myservoLeft.write(90 - (10  * (throttleLeft / |throttleLeft|)));
//        }
//
//        // Move Robot
//        myservoLeft.write(constrain(throttleLeft, 0, 180));
//        myservoRight.write(constrain(throttleRight, 0, 180));
//
//    }
//
//    // Return control to Comms A
//    return 0;
//
//}







void armExtend () {
  analogWrite(linearENA, motorStop);
  digitalWrite(linearIn1, HIGH);
  digitalWrite(linearIn2, LOW);
  analogWrite(linearENA, motorForward);

}

void armRetract() {
  analogWrite(linearENA, motorStop);
  digitalWrite(linearIn1, LOW);
  digitalWrite(linearIn2, HIGH);
  analogWrite(linearENA, motorForward);
}

void armStop() {
  analogWrite(linearENA, motorStop);
}

void beltStop() {
  analogWrite(beltENA, motorStop);
}

void beltRun() {
  analogWrite(beltENA, motorStop);
  digitalWrite(beltIn1, LOW);
  digitalWrite(beltIn2, HIGH);
  analogWrite(beltENA, motorForward);
}

void unclamp() {
  for (int pos = clampServo.read(); pos > 1; pos -= 1) {
    clampServo.write(pos);
    delay(5);
  }
}

void clamp() {
  for (int pos = clampServo.read(); pos < 181; pos += 1) {
    clampServo.write(pos);
    delay(5);
  }
}

int unlock() {
  digitalWrite(solenoidpin, LOW);  //sets the solenoid into LOW state(activates the relay for this board type).

  if (error == false) {
    delay(timer);
    lock(); //auto-locks once time is up.
    return 1;
  }

  else {
    error = true;
    return -1;
  }
}

int lock() {
  digitalWrite(solenoidpin, HIGH);  //sets the solenoid into HIGH state (deactivates the relay for this board type).
  return 1; //confirms that door has been locked
}

int lShapeGet(int num) {
  int numberOfCans = 0;

  boolean recent = false;

  armExtend();
  //turns the motor On

  Serial.println("hithere");
  int flexSensor = analogRead(sensor);
  float flexV = flexSensor * VCC / 1023.0;
  float flexR = R_DIV * (VCC / flexV - 1.0);
  // Use the calculated resistance to estimate the sensor's
  // bend angle:
  float angle = map(flexR, STRAIGHT_RESISTANCE, BEND_RESISTANCE,
                    0, 90.0);


  unsigned long baseTime = millis();
  unsigned long curTime = millis();

  while ((curTime - baseTime) <= 40000) {

    switchState1 = digitalRead(switch1);

    if (switchState1 != previousSwitchState1) {
      // if the state has changed,stop motor
      if (switchState1 == HIGH) {
        armStop();
        // waits for offTime milliseconds
        delay(500);
      }
      switchState1 = previousSwitchState1;
    }

    int flexADC = analogRead(sensor);
    float flexV = flexADC * VCC / 1023.0;
    float flexR = R_DIV * (VCC / flexV - 1.0);

    Serial.println("Resistance: " + String(flexR) + " ohms");

    // Use the calculated resistance to estimate the sensor's
    // bend angle:
    float angle = map(flexR, STRAIGHT_RESISTANCE, BEND_RESISTANCE,
                      0, 90.0);
    Serial.println("Bend: " + String(angle) + " degrees");
    Serial.println();

    delay(500);
    curTime = millis();
    if (angle >= threshold && recent == false) { //sensors get pressed

      numberOfCans++; //count++
      recent = true;
      Serial.println(numberOfCans);
      Serial.println(angle);
    }
    else if (angle < threshold && recent == true) {
      recent = false;
    }
    if (numberOfCans == num) {
      armStop();
      break;
    }
    if ((curTime - baseTime) >= 20000) {
      armStop();
      break;
    }
  }

  //buttonPressed or get enough cans
  delay(800);

  // turns the motor pin to another direction
  armRetract();
  baseTime = millis();
  curTime = millis();
  while ((curTime - baseTime) <= 10000) {
    curTime = millis();
    if ((curTime - baseTime) >= 10000) {
      armStop();
      break;
    }
    switchState2 = digitalRead(switch2);
    if (switchState2 != previousSwitchState2) {
      // if the state has changed,stop motor
      if (switchState2 == HIGH) {
        armStop();
        // waits for offTime milliseconds
        delay(500);
      }
      switchState2 = previousSwitchState2;
    }
  }
  return numberOfCans;
}

int rollerGet(int numOfCans) {
  int canCount = 0;
  armExtend();
  while (switchStateForward == previousSwitchStateForward) {
    Serial.println("forward");
    switchStateForward = digitalRead(forwardLimit);
  }
  armStop();
  clamp();
  Serial.println("clamp");
  switchStateCount = digitalRead(countSwitch);
  prevSwitchStateCount = switchStateCount;
  beltRun();
  Serial.println("belt run");
  long canTime = millis();
  long timeBetweenCan = millis();
  while (canCount < numOfCans * 2) {
    switchStateCount = digitalRead(countSwitch);
    timeBetweenCan = millis();

    if (timeBetweenCan - canTime > 10000) {
      break;
    }

    if (switchStateCount != prevSwitchStateCount) {
      canTime = timeBetweenCan;
      timeBetweenCan = millis();
      prevSwitchStateCount = switchStateCount;
      canCount++;
      delay(50);
      Serial.println("Pressed  ");
      Serial.println(canCount);
    }
  }
  beltStop();
  Serial.println("belt stop");
  unclamp();
  Serial.println("unclamp");
  delay(1000);
  armRetract();
  switchStateBack = digitalRead(rearLimit);
  previousSwitchStateBack = switchStateBack;
  while (switchStateBack == previousSwitchStateBack) {
    switchStateBack = digitalRead(rearLimit);
    Serial.println("Belt retracting");
  }
  Serial.println("Finished");
  armStop();
  return canCount / 2;
}


int getCans(int numOfCans) {
  // if(armState == true) { //roller flag
  int getCans = rollerGet(numOfCans);
  return getCans;
  // }
  // else if(armState == false) {
  // return lShapeGet(numOfCans);
  // }
}

void unload() {
  unlock();
}