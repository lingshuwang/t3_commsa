#pragma once

enum Status: unsigned char{
    HALT = 0,
    PICK = 1,
    DROP = 2,
};

class LU {
public:
    LU();
    void setStatus(unsigned char status);
    unsigned char getStatus();

    void setCount(unsigned char count);
    unsigned char getCount();

    void setRedNeed(unsigned char redNeed);
    void setGreenNeed(unsigned char greenNeed);
    void setBlueNeed(unsigned char blueNeed);
    unsigned char getRedNeed();
    unsigned char getGreenNeed();
    unsigned char getBlueNeed();

    void setTotal(unsigned char total);
    unsigned char getTotal();
    
    void beltRun();
    void beltStop();
    void clamp();
    void unclamp();
private:
    unsigned char status;
    unsigned char count;
    unsigned char redNeed;
    unsigned char greenNeed;
    unsigned char blueNeed;
    unsigned char total;
};
