#pragma once

enum Location: unsigned char {
	RED1 = 0,
	RED2 = 1,
	GREEN1 = 2,
	GREEN2 = 3,
	BLUE1 = 4,
	BLUE2 = 5,
	START = 6,
	END = 7,
};

enum MStatus: unsigned char{
	MHALT = 0,
	MOTION =1,
};

class Motion {
public:
	Motion();
	void setLocation(unsigned char location);
	void setCurrLocation(unsigned char currLocation);
	void setStatus(unsigned char status);
	unsigned char getLocation();
	unsigned char getCurrLocation();
	unsigned char getStatus();
private:
	unsigned char location;
	unsigned char currLocation;
	unsigned char status;
};
