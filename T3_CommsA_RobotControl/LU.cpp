#include "LU.h"

LU::LU() {
    count = 0;
    total = 0;
    redNeed = 0;
    greenNeed = 0;
    blueNeed = 0;
    status = HALT;
}

void LU::setStatus(unsigned char status) {
    this->status = status;
}
unsigned char LU::getStatus(){
    return this->status;
}

void LU::setCount(unsigned char count){
    this->count = count;
}
unsigned char LU::getCount() {
    return count;
}

void LU::setTotal(unsigned char total){
    this->total = total;
}
unsigned char LU::getTotal() {
    return total;
}

void LU::setRedNeed(unsigned char redNeed) {
    this->redNeed = redNeed;
}
void LU::setGreenNeed(unsigned char greenNeed) {
    this->greenNeed = greenNeed;
}
void LU::setBlueNeed(unsigned char blueNeed) {
    this->blueNeed = blueNeed;
}

unsigned char LU::getRedNeed() {
    return redNeed;
}
unsigned char LU::getGreenNeed() {
    return greenNeed;
}
unsigned char LU::getBlueNeed() {
    return blueNeed;
}

void LU::armExtend (){
  digitalWrite(linearIn1, HIGH);
  digitalWrite(linearIn2, LOW);
  analogWrite(linearENA, linearSpeedForward);
  
}

void LU::armRetract(){
  digitalWrite(linearIn1, LOW);
  digitalWrite(linearIn2, HIGH);
  analogWrite(linearENA, linearSpeedForward);
}

void LU::armStop(){
  analogWrite(linearENA, motorStop);
}

void LU::beltStop(){
  analogWrite(BeltEnA, motorStop);
}

void LU::beltRun(){
  analogWrite(BeltEnA, motorForward);
  digitalWrite(beltIn1, LOW);
  digitalWrite(beltIn2, HIGH);
}

void LU::unclamp(){
  for (int pos = clampServo.read(); pos >1; pos-= 1){
  clampServo.write(pos);
  delay(5);
  }
}

void LU::clamp(){
  for (int pos = clampServo.read(); pos <181; pos+= 1){
  clampServo.write(pos);
  delay(5);
  }
}

int LU::unlock(){
  digitalWrite(solenoidpin, HIGH);  //sets the solenoid into HIGH state

  if(error == false){
    delay(timer);
    lock(); //auto-locks once time is up.
    return 1;
   }

   else 
       error = true;
       return -1;
}

  int LU::lock(){
  digitalWrite(solenoidpin, LOW);  //sets the solenoid into LOW state
  return 1; //confirms that door has been locked
}

int LU::lShapeGet(int num){
  int numberOfCans = 0;

  boolean recent = false;

  armExtend();
  //turns the motor On
 
  Serial.println("hithere");
  int flexSensor = analogRead(sensor);
  float flexV = flexSensor * VCC / 1023.0;
  float flexR = R_DIV * (VCC / flexV - 1.0);
  // Use the calculated resistance to estimate the sensor's
  // bend angle:
  float angle = map(flexR, STRAIGHT_RESISTANCE, BEND_RESISTANCE,
                   0, 90.0);
  

  unsigned long baseTime = millis();
  unsigned long curTime = millis(); 

  while((curTime - baseTime) <= 40000){
  
  switchState1 = digitalRead(switch1);
  
  if (switchState1 != previousSwitchState1) {
  // if the state has changed,stop motor
    if (switchState1 == HIGH) {
      armStop(); 
      // waits for offTime milliseconds
      delay(500);
    }
    switchState1 = previousSwitchState1;
  }
  
  int flexADC = analogRead(sensor);
  float flexV = flexADC * VCC / 1023.0;
  float flexR = R_DIV * (VCC / flexV - 1.0);
  
  Serial.println("Resistance: " + String(flexR) + " ohms");

  // Use the calculated resistance to estimate the sensor's
  // bend angle:
  float angle = map(flexR, STRAIGHT_RESISTANCE, BEND_RESISTANCE,
                   0, 90.0);
  Serial.println("Bend: " + String(angle) + " degrees");
  Serial.println();

  delay(500);
    curTime = millis();
    if(angle >= threshold && recent == false){ //sensors get pressed 
      
      numberOfCans++; //count++
      recent = true;
      Serial.println(numberOfCans);
      Serial.println(angle);
    }
    else if(angle < threshold && recent == true){
      recent = false;
    }
    if(numberOfCans == num){
      armStop();
      break;
    }
    if ((curTime - baseTime) >= 20000) {
      armStop();
      break;
    }
  }
 
  //buttonPressed or get enough cans
  delay(800);

  // turns the motor pin to another direction
  armRetract();
  baseTime = millis();
  curTime = millis(); 
  while((curTime - baseTime) <= 10000){
    curTime = millis();
    if ((curTime - baseTime) >= 10000) {
      armStop();
      break;
    }
    switchState2 = digitalRead(switch2);
    if (switchState2 != previousSwitchState2) {
    // if the state has changed,stop motor
      if (switchState2 == HIGH) {
        armStop(); 
        // waits for offTime milliseconds
        delay(500);
      }
      switchState2 = previousSwitchState2;
    }
  }
  return numberOfCans;
}

int LU::rollerGet(int numOfCans) {
    int canCount = 0;
      armExtend();
      while(switchStateForward == previousSwitchStateForward) {
        Serial.println("forward"); 
        switchStateForward = digitalRead(forwardLimit);
      }
      armStop();
      clamp();
      Serial.println("clamp");
      switchStateCount = digitalRead(countSwitch);
      prevSwitchStateCount = switchStateCount;
      beltRun();
      Serial.println("belt run");
      while(canCount < numOfCans*2){
          switchStateCount = digitalRead(countSwitch);
          if(switchStateCount != prevSwitchStateCount) {
              prevSwitchStateCount = switchStateCount;
              count++;
              delay(50);
              Serial.println("Pressed  ");
              Serial.println(canCount);
          }
      }
    beltStop();
    Serial.println("belt stop");
    unclamp();
    Serial.println("unclamp");
    delay(1000);
    armRetract();
    switchStateBack = digitalRead(rearLimit);
    previousSwitchStateBack = switchStateBack;
    while(switchStateBack == previousSwitchStateBack){
      switchStateBack = digitalRead(rearLimit);
      Serial.println("Belt retracting");
    }
    Serial.println("Finished");
    armStop();
  return canCount;
}