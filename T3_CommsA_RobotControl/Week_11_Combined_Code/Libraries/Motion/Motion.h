#pragma once
#ifndef Motion_h
#define Motion_h

#include "Arduino.h"

enum Location: unsigned char {
	RED1 = 0,
	RED2 = 1,
	GREEN1 = 2,
	GREEN2 = 3,
	BLUE1 = 4,
	BLUE2 = 5,
	START = 6,
	END = 7,
};

enum MStatus: unsigned char{
	MHALT = 0,
	MOTION = 1
};

class Motion {
	public:
		Motion();
		void setLocation(unsigned char location);
		void setTargetLocation(unsigned char location);
		void setStatus(unsigned char status);
		unsigned char getLocation();
		unsigned char getTargetLocation();
		unsigned char getStatus();
	private:
		unsigned char _location;
		unsigned char _targetLocation;
		unsigned char _status;
		// Helper variables for Comms D
		int countLeft;
		int countRight;
		int target;
		int canLocations[7] = {200, 300, 600, 700, 1000, 1100, 0, 0};
};

#endif
