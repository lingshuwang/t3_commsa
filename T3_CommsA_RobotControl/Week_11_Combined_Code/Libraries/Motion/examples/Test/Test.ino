#include <Motion.h>

Motion motion;


void setup() {
  Serial.begin(9600);
  motion.setCurrLocation(RED2);
  motion.setTargetLocation(GREEN1);
  motion.setStatus(MOTION);
}

void loop() {
  Serial.print(motion.getTargetLocation());
  Serial.print('\t');
  Serial.print(motion.getCurrLocation());
  Serial.print('\t');
  Serial.print(motion.getStatus());
  Serial.print('\t');
  delay(1000);
}
