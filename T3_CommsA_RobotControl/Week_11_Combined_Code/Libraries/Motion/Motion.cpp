#include "Arduino.h"
#include "Motion.h"

Motion::Motion() {
    _location = START;
    _targetLocation = START;
    _status = MHALT;
}

void Motion::setLocation(unsigned char location){
    _location = location;
}
void Motion::setTargetLocation(unsigned char location) {
    _targetLocation = location;
}
void Motion::setStatus(unsigned char status) {
    _status = status;
}


unsigned char Motion::getLocation(){
    return _location;
}
unsigned char Motion::getTargetLocation(){
    return _targetLocation;
}
unsigned char Motion::getStatus(){
    return _status;
}
