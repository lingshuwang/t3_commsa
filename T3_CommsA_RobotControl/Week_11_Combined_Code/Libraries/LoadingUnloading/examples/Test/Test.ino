#include <LU.h>

LU l;

void setup() {
  Serial.begin(9600);
  l.setStatus(DROP);
  l.setCount(40);
  l.setRedNeed(250);
  l.setGreenNeed(252);
  l.setBlueNeed(251);
  l.setTotal(567);
}

void loop() {
  Serial.print(l.getStatus());
  Serial.print('\t');
  Serial.print(l.getCount());
  Serial.print('\t');
  Serial.print(l.getRedNeed());
  Serial.print('\t');
  Serial.print(l.getGreenNeed());
  Serial.print('\t');
  Serial.print(l.getBlueNeed());
  Serial.print('\t');
  Serial.println(l.getTotal());
  Serial.print('\t');
  delay(1000);
}
