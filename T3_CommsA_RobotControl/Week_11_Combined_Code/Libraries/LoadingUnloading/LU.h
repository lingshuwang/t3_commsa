#pragma once
#ifndef LU_h
#define LU_h

#include "Arduino.h"

enum Status: unsigned char{
    HALT = 0,
    PICK = 1,
    DROP = 2
};

class LU {

  public:
      LU();

      // Control whether Comms A or C has permission to run code
      void setStatus(unsigned char status);
      unsigned char getStatus();

      // TODO - Please write a small description of what this does
      void setCount(unsigned char count);
      unsigned char getCount();

      // Getters and setters for can data
      void setRedNeed(unsigned char redNeed);
      void setGreenNeed(unsigned char greenNeed);
      void setBlueNeed(unsigned char blueNeed);
      unsigned char getRedNeed();
      unsigned char getGreenNeed();
      unsigned char getBlueNeed();

      // TODO Is this setter necessary? Shouldn't you just add them together based on redNeed ...
      void setTotal(unsigned char total);
      unsigned char getTotal();
  private:
      unsigned char _status;
      unsigned char _count;
      unsigned char _redNeed;
      unsigned char _greenNeed;
      unsigned char _blueNeed;
      unsigned char _total;

};

#endif
