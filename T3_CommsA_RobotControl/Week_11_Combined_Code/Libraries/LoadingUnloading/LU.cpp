#include "Arduino.h"
#include "LU.h"

// Initilise variables
LU::LU() {
    _count = 0;
    _total = 0;
    _redNeed = 0;
    _greenNeed = 0;
    _blueNeed = 0;
    _status = HALT;
}

void LU::setStatus(unsigned char status) {
    _status = status;
}
unsigned char LU::getStatus() {
    return _status;
}

void LU::setCount(unsigned char count) {
    _count = count;
}
unsigned char LU::getCount() {
    return _count;
}

void LU::setTotal(unsigned char total){
    _total = total;
}
unsigned char LU::getTotal() {
    return _total;
}

void LU::setRedNeed(unsigned char redNeed) {
    _redNeed = redNeed;
}
void LU::setGreenNeed(unsigned char greenNeed) {
    _greenNeed = greenNeed;
}
void LU::setBlueNeed(unsigned char blueNeed) {
    _blueNeed = blueNeed;
}

unsigned char LU::getRedNeed() {
    return _redNeed;
}
unsigned char LU::getGreenNeed() {
    return _greenNeed;
}
unsigned char LU::getBlueNeed() {
    return _blueNeed;
}
