# T3_CommsA_RobotControl

Motions.h
The interface between motion and control system. The control system will give an unsigned char containing the following;
enum colour 
unsigned char RED1 = 0;
unsigned char RED2 = 1;
unsigned char GREEN1 = 2; 
unsigned char GREEN2 = 3;
unsigned char BLUE1 = 4; 
unsigned char BLUE2 = 5;
unsigned char START =6 ;
unsigned char END = 7;
}
Status of the motions team will be controlled by enum status { unsigned char (HALT = 0, MOTION=  1). }
When motions is not required to do anything, 0 will be stored in status, if motions is required to do something, status will become 1. 
The following functions will be available for motions to call at anytime;
setStatus(0); (can be used by motions to change status from MOTION → HALT when they have completed the task, cannot be changed by motions from HALT → MOTION)
getLocation(); (can be used to see which colour needs to be moved to next)
getStatus(); (motions can check continuously whether status == 1, so then they know to start moving)
getCurrLocation(); 
setCurrLocation(unsigned char);

LU.h (loading/unloading)
Colour count of a single order will be stored in global variables called redNeed, blueNeed, greenNeed
When cans get collected a enum called Status where;
HALT = 0
PICK = 1;
DROP = 2;
Commands between each are controlled by enum status { HALT = 0, PickupSTART =1, DROPOFF =2 }
The following functions will be avaliable for Loading/unloading to call at anytime;
setStatus(0); set to 0 when finished work, if 1 or 2 then that means loading/ unloading will begin
getStatus();
Once the cans have been collected, the total will be incremented which stands for total collected, greenNeed, redNeed and blueNeed will be updated showing how many cans have been collected,
An array of red1, red2, green1, green2, blue1, blue2 will be used called inv[6] where 
inv[0] = RED1 location (0-4)
inv[1] = RED2 location (0-4)
inv[2] = GREEN1 location (0-4)
inv[3] = GREEN2 location (0-4)
inv[4] = BLUE1 location (0-4)
inv[5] = BLUE2 location (0-4)
currCount of each colour will be used to determine how many more cans will be needed to collect