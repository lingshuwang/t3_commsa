#include "gtest/gtest.h"
#include "LU.h"

TEST(LUTest, Constructor) {
    LU *l = new LU();
    EXPECT_EQ(0, l->getCount());
    EXPECT_EQ(0, l->getTotal());
    EXPECT_EQ(0, l->getRedNeed());
    EXPECT_EQ(0, l->getGreenNeed());
    EXPECT_EQ(0, l->getBlueNeed());
    EXPECT_EQ(HALT, l->getStatus());

}

//Count
TEST(LUTest, Count) {
    LU *l = new LU();
    l->setCount(3);
    EXPECT_EQ(3, l->getCount());
    l->setCount(0);
    EXPECT_EQ(0, l->getCount());
}

//Total
TEST(LUTest, Total) {
    LU *l = new LU();
    l->setTotal(10);
    EXPECT_EQ(10, l->getTotal());
    l->setTotal(0);
    EXPECT_EQ(0, l->getTotal());
}

//Red
TEST(LUTest, set_RedNeed) {
    LU *l = new LU();
    l->setRedNeed(5);
    EXPECT_EQ(5, l->getRedNeed());
    l->setRedNeed(1);
    EXPECT_EQ(1, l->getRedNeed());
}

TEST(LUTest, get_RedNeed) {
    LU *l = new LU();
    l->setRedNeed(6);
    EXPECT_EQ(6, l->getRedNeed());
    l->setRedNeed(2);
    EXPECT_EQ(2, l->getRedNeed());
}

//Green
TEST(LUTest, set_GreenNeed) {
    LU *l = new LU();
    l->setGreenNeed(5);
    EXPECT_EQ(5, l->getGreenNeed());
    l->setGreenNeed(1);
    EXPECT_EQ(1, l->getGreenNeed());
}

TEST(LUTest, get_GreenNeed) {
    LU *l = new LU();
    l->setRedNeed(6);
    EXPECT_EQ(6, l->getRedNeed());
    l->setRedNeed(2);
    EXPECT_EQ(2, l->getRedNeed());
}

//Blue
TEST(LUTest, set_BlueNeed) {
    LU *l = new LU();
    l->setBlueNeed(5);
    EXPECT_EQ(5, l->getBlueNeed());
    l->setBlueNeed(1);
    EXPECT_EQ(1, l->getBlueNeed());
}

TEST(LUTest, get_BlueNeed) {
    LU *l = new LU();
    l->setBlueNeed(6);
    EXPECT_EQ(6, l->getBlueNeed());
    l->setBlueNeed(2);
    EXPECT_EQ(2, l->getBlueNeed());
}

//Status
TEST(LUTest, Status) {
    LU *l = new LU();
    l->setStatus(HALT);
    EXPECT_EQ(HALT, l->getStatus());
    l->setStatus(PICK);
    EXPECT_EQ(PICK, l->getStatus());
    l->setStatus(DROP);
    EXPECT_EQ(DROP, l->getStatus());
}
