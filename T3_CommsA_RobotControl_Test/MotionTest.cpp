#include "gtest/gtest.h"
#include "Motion.h"

TEST(MotionTest, Constructor){
    Motion *m = new Motion();
    EXPECT_EQ(START, m->getLocation());
    EXPECT_EQ(START, m->getCurrLocation());
    EXPECT_EQ(MHALT, m->getStatus());
}

TEST(MotionTest, set_Location){
    Motion *m = new Motion();
    m->setLocation(RED1);
    EXPECT_EQ(RED1, m->getLocation());
    m->setLocation(RED2);
    EXPECT_EQ(RED2, m->getLocation());
}

TEST(MotionTest, get_location){
    Motion *m = new Motion();
    m->setLocation(GREEN1);
    EXPECT_EQ(GREEN1, m->getLocation());
    m->setLocation(GREEN2);
    EXPECT_EQ(GREEN2, m->getLocation());
}

TEST(MotionTest, set_currLocation){
    Motion *m = new Motion();
    m->setCurrLocation(RED1);
    EXPECT_EQ(RED1, m->getCurrLocation());
    m->setCurrLocation(RED2);
    EXPECT_EQ(RED2, m->getCurrLocation());
}

TEST(MotionTest, get_currLocation){
    Motion *m = new Motion();
    m->setCurrLocation(BLUE1);
    EXPECT_EQ(BLUE1, m->getCurrLocation());
    m->setCurrLocation(BLUE2);
    EXPECT_EQ(BLUE2, m->getCurrLocation());
}

TEST(MotionTest, Status){
    Motion *m = new Motion();
    m->setStatus(MOTION);
    EXPECT_EQ(MOTION, m->getStatus());
    m->setStatus(MHALT);
    EXPECT_EQ(MHALT, m->getStatus());
}