#include "RQ.h"

RQ::RQ() {
    head = 0;
    tail = 0;
    numReqs = 0;
    numRQOs = 0;
    for (int i=0; i<MAX_RQOS; i++) {
        rqos[i] = 0;
    }
}

RQ::~RQ() {}

void RQ::subscribe(RQO *rqo) {
    if (numRQOs >= MAX_RQOS) {
        return;
    }
    rqos[numRQOs] = rqo;
    numRQOs++;
}

void RQ::add(Req *req) {
    if (req->getStatus() != WRITE || numReqs >= MAX_REQS) return;
    if (tail == 0) {
        req->prev = 0;
        req->next = 0;
        head = req;
        tail = req;
        req->setStatus(ACTIVE);
        for (int i=0; i<numRQOs; i++) rqos[i]->notify(req);
    }
    else {
        tail->next = req;
        req->prev = tail;
        req->next = 0;
        tail = req;
        req->setStatus(WAIT);
        for (int i = 0; i < numRQOs; i++) rqos[i]->notify(req);
    }
    numReqs++;
}

void RQ::setStatus(Req *req, unsigned char status) {
    if (status == WRITE || req == 0 || (req->getStatus() == ACTIVE && status == CANCELLED)) return;
    if (req != 0) {
        req->setStatus(status);
        for (int i=0; i<numRQOs; i++) rqos[i]->notify(req);
        if (status != ACTIVE && status != WAIT) {
            Req *prev = req->prev;
            Req *next = req->next;
            if (prev != 0) prev->next = next;
            if (next != 0) next->prev = prev;
            if (req == head) {
                head = next;
                if (next != 0) {
                    next->setStatus(ACTIVE);
                    for (int i=0; i<numRQOs; i++) rqos[i]->notify(next);
                }
            }
            if (req == tail) tail = prev;
            req->~Req();
            numReqs--;
        }
    }
}

void RQ::setStatus(unsigned char rID, unsigned char status) {
    setStatus(get(rID), status);
}

Req * RQ::get() {
    return head;
}

Req * RQ::get(unsigned char rID) {
    Req *cursor = head;
    while (cursor != 0) {
        if (cursor->getID() == rID) {
            return cursor;
        }
        cursor = cursor->next;
    }
    return 0;
}