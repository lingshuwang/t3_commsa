#pragma once

enum Product : unsigned char {
    RED = 0,
    GREEN = 1,
    BLUE = 2,
    NUM_PRODS = 3,
};

enum Status : unsigned char {
    WRITE = 0,
    WAIT = 1,
    ACTIVE = 2,
    SUCCESS = 3,
    CANCELLED = 4
};

class Req {
    public:
        Req *prev;
        Req *next;
        Req();
        ~Req();
        void setQuant(unsigned char pID, unsigned char quant);
        void setStatus(unsigned char status);
        unsigned char getID();
        unsigned char getStatus();
        unsigned char getQuant(unsigned char pID);
    private:
        unsigned char id;
        unsigned char status;
        unsigned char quants[NUM_PRODS];
};