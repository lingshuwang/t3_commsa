#include "Req.h"

static unsigned char serial = 0;

Req::Req() {
    id = serial;
    serial++;
    status = WRITE;
    for (int i=0; i<NUM_PRODS; i++) {
        quants[i] = 0;
    }
    prev = 0;
    next = 0;
}

Req::~Req() {}

void Req::setQuant(unsigned char pID, unsigned char quant) {
    if (status != WRITE || pID >= NUM_PRODS) return;
    quants[pID] = quant;
}

void Req::setStatus(unsigned char status) {
    if (status == WRITE) return;
    this->status = status;
}

unsigned char Req::getID() {
    return id;
}

unsigned char Req::getStatus() {
    return status;
}

unsigned char Req::getQuant(unsigned char pID) {
    if (pID >= NUM_PRODS) return 0;
    return quants[pID];
}