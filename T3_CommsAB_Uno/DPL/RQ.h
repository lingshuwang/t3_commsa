#pragma once

#include "Req.h"

const unsigned char MAX_REQS = 16;
const unsigned char MAX_RQOS = 2;

class RQO {
    public:
        virtual void notify(Req *req) = 0;
};

class RQ {
    public:
        RQ();
        ~RQ();
        void subscribe(RQO *rqo);
        void add(Req *req);
        void setStatus(Req *req, unsigned char status);
        void setStatus(unsigned char rID, unsigned char status);
        Req * get();
        Req * get(unsigned char rID);
    private:
        Req *head;
        Req *tail;
        RQO *rqos[MAX_RQOS];
        unsigned char numRQOs;
        unsigned char numReqs;
};