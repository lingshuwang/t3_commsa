void clearBuffer() {
  while (BTserial.available() > 0) {BTserial.read();}
}


bool establishConnectionToRobot() {
/* Sets up initial connection between to robot, by testing for an exisiting connection,
 * checking if that connection is with the valid device. 
 * 
 * If a connection does not exist the HM-10 module is configured using AT commands
 * (assumes any exisiting configuration is incorrect).
 * 
 * Server is setup as master.
 * 
 * Returns true if connection is established, false otherwise.
 * 
 */

  // Used as a reference time with millis(); function for timeouts
  unsigned long refTime;

  // Sets up state pin
  pinMode(7, INPUT);

  // Set up baud rate on software serial ports
  BTserial.begin(9600);

  clearBuffer();

  // Checks for current connection
  refTime = millis();
  while (millis() - refTime < 1000) {
    if (digitalRead(7) == LOW) {

      // No current connection, begin configuring module
      int numAtCommands = 8;
      String startAtCommands[numAtCommands] = {"AT+RENEW", "AT+ALLO1", "AT+AD1D43639DB0B00", "AT+IMME0", "AT+ROLE1", "AT+TYPE2", "AT+PASS676767", "AT+RESET"};

      // Sets up bluetooth module
      for (int i = 0; i < numAtCommands; i++) {
        BTserial.print(startAtCommands[i]);
        BTserial.flush();
        // Ensures that module is responding to AT commands, if not it may already be connected
        refTime = millis();
        while (BTserial.available() < startAtCommands[i].length()) {
          if (millis() - refTime > 2000) {
            break;
          }
        }
       clearBuffer();
      }
      break;
    }
  }
  // Once a connection has been established, it checks we are connected to the right device
  return testConnection();
}

bool testConnection() {
  /* Tests for a valid connection by ensuring the response for a test message
   * is the predefined valid one for this system.
   *
   * Returns true if expected response is found, false otherwise.
   */
  // Timeout between waiting for a single response, in ms
  BTserial.setTimeout(700);

  for (int i = 1; i <= 10 /* Num of times test message is sent */; i++) {
    BTserial.write(requestTest);
    // Ensures both sides can run testConnection at the same time.
    if (BTserial.peek() == requestTest) {clearBuffer(); BTserial.write(replyTest);}
    if (BTserial.find(replyTest)) {
      clearBuffer();
      return true;
    }
  }
    clearBuffer();
    return false;
}

void BTserialEvent() {
/* Deals with actioning all messages, and is a function called when there is
 * data in the serial buffer that needs to be dealt with.
 *
 * This function should only be called if BTserial.available() is true.
 *
 * This function should be called whenever we are waiting for a message or inbetween
 * actions to ensure a response to incoming messages.
 *
 */

  // Test message in serial buffer
  if (BTserial.peek() == requestTest) {
    BTserial.read();
    BTserial.write(replyTest);
  }

  // Reading in messages from buffer
  //--------------------------------

  // Timeout for waiting for full message, in ms
  BTserial.setTimeout(250);

  // Stores current message being read in
  String currentMessage;

  // Job complete message type -- 2 characters long
  if (BTserial.peek() == 'D') {
    if (BTserial.available() >= 2) {return;}
    currentMessage = BTserial.readStringUntil('>');
  } else {BTserial.read();}

  // Actioning messages
  //-------------------

  // Ensures that there is a message stored in currentMessage
  if (currentMessage.length() > 0) {


    // Done message
    if (currentMessage[0] == 'D') {
      Serial.println("Wm:D");
      rq->setStatus(rID, SUCCESS);
    }
     else Serial.println("Wm:U");
  }
  
  currentMessage = "";
  
}

void sendCan(unsigned char R, unsigned char G, unsigned char B) {
 String message = "C" + String(R) + String(G) + String(B) + "c>";
 BTserial.print(message);
 BTserial.flush();
}
