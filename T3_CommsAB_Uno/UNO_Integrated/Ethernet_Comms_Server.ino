void establishConnectionToClient() {
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.begin(9600);
  if (!SD.begin(4)) return;
  if (!SD.exists("p.htm")) return;
}

void pollClient() {
  EthernetClient client = server.available();
  if (client) while (client.connected()) if (client.available()) {
    char c = client.read();
    if (readString.length() < 100) {
      readString += c;
    }
    if (c == '\n') {
      Serial.println(readString);
      client.println("HTTP/1.1 200 OK");
      client.println("Content-Type: text/html");
      client.println();
      File webFile = SD.open("p.htm");
      if (webFile) {
        while (webFile.available()) {
          client.write(webFile.read());
        }
        webFile.close();
      }
      delay(1);
      client.stop();
      if (readString.startsWith("GET /?")) {
          Req *r = new Req();
          for (int i=0; i<NUM_PRODS; i++) {
            int index = readString.indexOf(prodStrings[i]);
            if (index <= 0) continue;
            String asString = readString.substring(index+3, index+4);
            if (asString.charAt(1) == 48) r->setQuant(i, 10);
            else r->setQuant(i, asString.charAt(0) - 48);
          }
          rq->add(r);
      }
      //readString.~String();
      readString = "";
    }
  }
}
