#include <AltSoftSerial.h>
#include <RQ.h>
#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>

// Server-client:
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(169,254,212,122);
EthernetServer server(80);
String readString = "";
String prodStrings[3] = {"rc", "gc", "bc"};

// DPL:
RQ *rq;
unsigned char rID;

// Bluetooth:

AltSoftSerial BTserial;
unsigned char requestTest = 'T';
unsigned char replyTest   = 't';


// Bluetooth functions: 

void clearBuffer();
bool establishConnectionToRobot();
bool testConnection();
void BTserialEvent();
void sendCan(unsigned char R, unsigned char G, unsigned char B);


// Ethernet functions:
void establishConnectionToClient();
void pollClient();

class Observer : public RQO {
public:
    Observer() {}
    void notify (Req *req) {
        if (req->getStatus() == ACTIVE) {
            rID = req->getID();
            sendCan(req->getQuant(RED), req->getQuant(GREEN), req->getQuant(BLUE));
        }
        Serial.println();
        Serial.println(req->getID());
        Serial.println(req->getStatus());
        Serial.println(req->getQuant(RED));
        Serial.println(req->getQuant(GREEN));
        Serial.println(req->getQuant(BLUE));
    }
};

void setup() {
    Serial.begin(9600);

    if (!establishConnectionToRobot()) {
        Serial.println("Wc:F");
        Serial.flush();
        setup();
    }
    else {
        Serial.println("Wc:P");
    }

    pinMode(4, OUTPUT);
    digitalWrite(4, LOW);
    establishConnectionToClient();
    rq = new RQ();
    rq->subscribe(new Observer());
}

void loop() {
    pollClient();
    if (Serial.available() > 0) BTserial.write(Serial.read());
    if (BTserial.available()) BTserialEvent();
}
