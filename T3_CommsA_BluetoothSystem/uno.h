#pragma once

class Uno {
public:
    Uno();

    bool establishConnectionToRobot();

    bool testConection();

    void BTserialEvent();

    bool replyToTest();

    void storeCanInfo(String canMessage);

    void sendInitalAck();

    void sendSecondAck();

    void sendCan(int R, int G, int B);
}
