#include "Mega.h"

Mega::Mega() {
    status = NOTHING;
}

bool Mega::establishConnectionToServer() {
    /* Sets up initial connection to server via bluetooth modules using AT commands.
     * Removes previous settings and uses a temp pass to prevent connection to allow
     * for correct values to be set, then sets pass to commonPass, should be the same
     * on both ends of the communcation.
     *
     * Robot is set as slave using AT+ROLE0
     *
     * Returns true if connection established, false otherwise
      */

    //Set up serial ports on arduino for serial monitor
    Serial.begin(9600);
    //Prints to serial monitor
    Serial.print("Sketch:   ");   Serial.println(__FILE__);
    Serial.print("Uploaded: ");   Serial.println(__DATE__);
    Serial.println(" ");

    //Set up serial ports connected to bluetooth module
    Serial1.begin(9600);

    // Checks to see if connection is already present
    bool connected = true;
    pinMode(statePin, INPUT);
    unsigned long refTime = millis();
    int timeoutState = 1000;  // in ms

    while (millis() - refTime < timeoutState) {
        if (digitalRead(statePin) == LOW) {
            connected = false;
            Serial.println("Connection not present, establishing new connection.");  // Only used to print messages in monitor
            break;
        } else {
            Serial.println("Currently connected, awaiting timeout (" + String(millis() - refTime) + " / " + String(timeoutState) + ").");   // Only used to print messages in monitor
        }
    }
    if (!connected) {
        int numAtCommands = 8;
        String startAtCommands[numAtCommands] = {"AT+RENEW", "AT+RESET","AT+ALLO1", "AT+AD1"+serverAddress ,"AT+ROLE0", "AT+TYPE2", "AT+PASS"+commonPass, "AT+RESET"};

        //Sets up bluetooth module
        for (int i = 0; i < numAtCommands; i++) {
            Serial1.print(startAtCommands[i]);
            Serial.print(startAtCommands[i]);                             // Only used to print messages in monitor
            // Creates required delay for module to process commands

            refTime = millis();
            int timeoutBTProcess = 2000;  // in ms
            while (Serial1.available() < startAtCommands[i].length()) {  // Could possibly use Serial1.flush(); here
                delay(1);
                if (millis() - refTime > timeoutBTProcess) {
                    Serial.println("Timeout: Module not responding to AT commands, attempting to test connection.");
                    break;
                }
            }
            Serial.println(" ");                                          // Only used to print messages in monitor
            while (Serial1.available() > 0) {                            // Only used to print messages in monitor
                Serial.write(Serial1.read());                              // Only used to print messages in monitor
            }                                                             // Only used to print messages in monitor
            Serial.println(" ");                                          // Only used to print messages in monitor
        }
    }
    return testConnection();
}

bool Mega::testConnection() {
    // Confirms we are connected to the correct device
    int timeOutIteration = 2000; // in ms
    Serial1.setTimeout(timeOutIteration);
    for (int i = 1; i <= 10 /* Num of times test message is sent */; i++) {
        Serial1.write(requestTest);
        replyToTest();    // Incase both ends are testing at the same time
        if (Serial1.find(replyTest)) {
            return true;
        } else {                                                                          // Only used to print messages in monitor
            Serial.println("Timeout: Did not detect expected message, attempt " + String(i));       // Only used to print messages in monitor
        }
    }
    return false;
}

void Mega:: serialEvent1() {
    if (!replyToTest()) {
        // Undefined message type or user input
        //Serial.write(Serial1.read());       // Only used to print messages in monitor

    }

    String currentMessage;
    Serial1.setTimeout(5000);
    if (Serial1.peek() == 'C') { // Can message -- 5 characters
        if (Serial1.available() < 5) { // Include timeout conidition later<<<<<<<<<<<<<<<<<<<<<<<
            Serial.println("Can message sensed, waiting for full message");
            return;
        }

        currentMessage = Serial1.readStringUntil('c');

    } else if (Serial1.peek() == 'A') { // Initial Ack -- 3 characters
        if (Serial1.available() < 3) {
            Serial.println("Inital Ack message sensed, waiting for full message");
            return;
        }

        currentMessage = Serial1.readStringUntil('a');

    } else if (Serial1.peek() == 'K') { // Second Ack -- 3 characters
        if (Serial1.available() < 3) {
            Serial.println("Second Ack message sensed, waiting for full message");

            return;
        }

        currentMessage = Serial1.readStringUntil('k');

    } else {  // Undefined character or incomplete message

        Serial1.read();

    }

    if (currentMessage.length() > 0) { // Process messages

        if (currentMessage[0] == 'C') { // Can message
            // Store values, then reply with an inital ACK, only run when sending can message from server --> robot
            storeCanInfo(currentMessage);
            sendInitialAck();
            Serial.println("Can message recieved, sending back inital ACK, storing values.");

        } else if (currentMessage[0] == 'A') { // Inital Ack message
            // Reply with a Second ACK, only run when sending can message from robot --> server
            sendSecondAck();
            robotListening = !robotListening;
            Serial.println("Inital ACK recieved, sending back second ACK, robotListening = " + String(robotListening));

        } else if (currentMessage[0] == 'K') { // Second Ack message
            // Conlcude request, change robot listening status, only run when sending can message from server --> robot
            robotListening = !robotListening;
            Serial.println("Second ACK recieved, robotListening = " + String(robotListening));

        }

        lastRecieved = currentMessage;
        currentMessage = "";
    }
}

bool Mega::replyToTest() {
    if (Serial1.peek() == requestTest) {    // Test message recieved
        Serial1.read();
        Serial1.write(replyTest);
        Serial.println("Test message received, responding");                   // Only used to print messages in monitor
        return true;
    }
    return false;
}
void Mega::storeCanInfo(String canMessage) {
    //to complete
}
void Mega:: sendInitialAck() { // Only sends acks for can messages, need to expand later
    Serial1.print("ACa");
    Serial1.flush();
}
void Mega::sendSecondAck() {
    Serial1.print("KCk");
    Serial1.flush();
}
void Mega::sendCan(int R, int G, int B) {
    String message = "C" + String(R) + String(G) + String(B) + "c";
    Serial1.print(message);
    Serial1.flush();
}

void setStatus(unsigned char status){
    this->status = status;
}

unsigned char getStatus(){
   return status;
}
