//  Pins
//  BT VCC to Arduino 5V out.
//  BT GND to GND
//  Arduino D8 (SS RX) - BT TX no need voltage divider
//  Arduino D9 (SS TX) - BT RX through a voltage divider (5v to 3.3v)
//  Arduino D10 - BT State

#include <AltSoftSerial.h>
AltSoftSerial BTserial;

#include <RQ.h>
RQ.h ;

#include <Req.h>
Req.h;

unsigned char rID;
String commonPass = "676767";
String serverAddress = "D43639DB06FF";
String robotAddress = "D43639DB0B00";
unsigned char requestTest = 'T';
unsigned char replyTest   = 't';
unsigned int statePin = 10;

String lastMessage;

bool establishConnectionToRobot() {
  /* Sets up initial connection between to robot, by testing for an exisiting connection,
     checking if that connection is with the valid device.

     If a connection does not exist the HM-10 module is configured using AT commands
     (assumes any exisiting configuration is incorrect).

     Server is setup as master.

     Returns true if connection is established, false otherwise.

  */
  // Sets up state pin
  pinMode(statePin, INPUT);

  // Timeouts used in respective sections, all in ms
  unsigned int currentConnectionTimeout = 1000;
  unsigned int ATcommandsTimeout = 2000;

  // Used as a reference time with millis(); function for timeouts
  unsigned long refTime;

  // Set up baud rate on software serial ports
  BTserial.begin(9600);

  // Checks for current connection
  refTime = millis();
  while (millis() - refTime < currentConnectionTimeout) {
    if (digitalRead(statePin) == LOW) {

      // No current connection, begin configuring module
      int numAtCommands = 9;
      String startAtCommands[numAtCommands] = {"AT+RENEW", "AT+RESET", "AT+ALLO1", "AT+AD1" + robotAddress, "AT+IMME0", "AT+ROLE1", "AT+TYPE2", "AT+PASS" + commonPass, "AT+RESET"};

      // Sets up bluetooth module
      for (int i = 0; i < numAtCommands; i++) {
        BTserial.print(startAtCommands[i]);
        BTserial.flush();

        // Ensures that module is responding to AT commands, if not it may already be connected
        refTime = millis();
        while (BTserial.available() < startAtCommands[i].length()) {
          if (millis() - refTime > ATcommandsTimeout) {
            break;
          }
        }
      }
      break;
    }
  }
  // Once a connection has been established, it checks we are connected to the right device
  return testConnection();
}

boolean testConnection() {
  /* Tests for a valid connection by ensuring the response for a test message
     is the predefined valid one for this system.

     Returns true if expected response is found, false otherwise.
  */

  // Timeout between waiting for a single response, in ms
  BTserial.setTimeout(2000);

  for (int i = 1; i <= 10 /* Num of times test message is sent */; i++) {
    BTserial.write(requestTest);
    // Ensures both sides can run testConnection at the same time.
    if (BTserial.peek() == requestTest) {
      BTserial.read();
      BTserial.write(replyTest);
    }
    if (BTserial.find(replyTest)) {
      return true;
    }
  }
  return false;
}

void setup() {
  // put your setup code here, to run once:
  establishConnectionToRobot();
  pinMode(4, OUTPUT);
  digitalWrite(4, LOW);
  //
  RQ *rq;
  rq -> subscribe(new Observer());

}

void loop() {
  // put your main code here, to run repeatedly:
  if (BTserial.available()) {
    BTserialEvent();
  }
}

void BTserialEvent() {
  /* Deals with actioning all messages, and is a function called when there is
     data in the serial buffer that needs to be dealt with.

     This function should only be called if BTserial.available() is true.

     This function should be called whenever we are waiting for a message or inbetween
     actions to ensure a response to incoming messages.

  */

  // Test message in serial buffer
  if (BTserial.peek() == requestTest) {
    BTserial.read();
    BTserial.write(replyTest);
    BTserial.flush();
  }

  // Reading in messages from buffer
  //--------------------------------

  // Timeout for waiting for full message, in ms
  BTserial.setTimeout(250);

  // Stores current message being read in
  String currentMessage;

  // Job complete message type -- 2 characters long
  if (BTserial.peek() == 'D') {
    if (BTserial.available() >= 2) {
      return;
    }
    currentMessage = BTserial.readStringUntil('d');
    // Inital ACK message type -- 3 characters long
  } else {
    BTserial.read();
  }


  // Actioning messages
  //-------------------

  // Ensures that there is a message stored in currentMessage
  if (currentMessage.length() > 0) {

    // Can message
    if (currentMessage[0] == 'D') {

      // Update job status
      rq->setStatus(rID, SUCCESS); //req has to be pointer
    }
  }

  // Clears currentMessage, stores in lastMessage
  lastMessage = currentMessage;
  currentMessage = "";

}

void sendCan(unsigned int R, unsigned int G, unsigned int B) {
  String message = "C" + String(R) + String(G) + String(B) + "c";
  BTserial.print(message);
  BTserial.flush();
}

class observer : public RQO {
    observer() {}

    void notify (Req *req) {
      if (req.getStatus() == ACTIVE) {
        unsigned char red = req.getQuant(RED);
        unsigned char Green = req.getQuant(GREEN);
        unsigned char Blue = req.getQuant(BLUE);
        rID=req.getID();
        sendCan(red, green, blue);
      }
    }
}