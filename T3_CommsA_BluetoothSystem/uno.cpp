#include <AltSoftSerial.h>
#include "Uno.h"
Uno::Uno() {

}

bool establishConnectionToRobot() {
    /* Sets up initial connection to robot via bluetooth modules using AT commands.
     * Removes previous settings and uses a temp pass to prevent connection to allow
     * for correct values to be set, then sets pass to commonPass, should be the same
     * on both ends of the communcation.
     *
     * Server is set to master using AT+ROLE1
     *
     * Returns true if connection established, false otherwise
     *
      */

    //Set up serial ports on arduino for serial monitor
    Serial.begin(9600);
    //Prints to serial monitor
    Serial.print("Sketch:   ");   Serial.println(__FILE__);
    Serial.print("Uploaded: ");   Serial.println(__DATE__);
    Serial.println(" ");

    //Set up software serial ports connected to bluetooth module
    BTserial.begin(9600);

    // Checks to see if connection is already present
    bool connected = true;
    pinMode(statePin, INPUT);
    unsigned long refTime = millis();
    int timeoutState = 1000;  // in ms

    while (millis() - refTime < timeoutState) {
        if (digitalRead(statePin) == LOW) {
            connected = false;
            Serial.println("Connection not present, establishing new connection.");  // Only used to print messages in monitor
            break;
        } else {
            Serial.println("Currently connected, awaiting timeout (" + String(millis() - refTime) + " / " + String(timeoutState) + ").");   // Only used to print messages in monitor
        }
    }
    if (!connected) {
        int numAtCommands = 9;
        String startAtCommands[numAtCommands] = {"AT+RENEW", "AT+RESET", "AT+ALLO1", "AT+AD1"+robotAddress, "AT+IMME0", "AT+ROLE1", "AT+TYPE2", "AT+PASS"+commonPass, "AT+RESET"};

        //Sets up bluetooth module
        for (int i = 0; i < numAtCommands; i++) {
            BTserial.print(startAtCommands[i]);
            Serial.print(startAtCommands[i]);                             // Only used to print messages in monitor
            // Creates required delay for module to process commands

            refTime = millis();
            int timeoutBTProcess = 2000;  // in ms
            while (BTserial.available() < startAtCommands[i].length()) {  // Could possibly use Serial1.flush(); here
                delay(1);
                if (millis() - refTime > timeoutBTProcess) {
                    Serial.println("Timeout: Module not responding to AT commands, attempting to test connection.");
                    break;
                }
            }

            Serial.println(" ");                                          // Only used to print messages in monitor
            while (BTserial.available() > 0) {                            // Only used to print messages in monitor
                Serial.write(BTserial.read());                              // Only used to print messages in monitor
            }                                                             // Only used to print messages in monitor
            Serial.println(" ");                                          // Only used to print messages in monitor
        }
    }
    return testConnection();
}
bool Uno::testConnection() {
    // Confirms we are connected to the correct device
    int timeOutIteration = 2000; // in ms
    BTserial.setTimeout(timeOutIteration);
    for (int i = 1; i <= 10 /* Num of times test message is sent */; i++) {
        BTserial.write(requestTest);
        replyToTest();    // Incase both ends are testing at the same time
        if (BTserial.find(replyTest)) {
            return true;
        } else {                                                                          // Only used to print messages in monitor
            Serial.println("Timeout: Did not detect expected message, attempt " + String(i));       // Only used to print messages in monitor
        }
    }
    return false;
}
void Uno::BTserialEvent() {
    if (!replyToTest()){
        // Undefined message type or user input
        //Serial.write(BTserial.read());                                                    // Only used to print messages in monitor
    }
    String currentMessage;
    BTserial.setTimeout(5000); // Timeout for reading message, in ms
    if (BTserial.peek() == 'C') { // Can message -- 5 characters
        if (BTserial.available() < 5) { // Include timeout conidition later<<<<<<<<<<<<<<<<<<<<<<<
            Serial.println("Can message sensed, waiting for full message");
            return;
        }

        currentMessage = BTserial.readStringUntil('c');

    } else if (BTserial.peek() == 'A') { // Initial Ack -- 3 characters
        if (BTserial.available() < 3) {
            Serial.println("Inital Ack message sensed, waiting for full message");
            return;
        }

        currentMessage = BTserial.readStringUntil('a');

    } else if (BTserial.peek() == 'K') { // Second Ack -- 3 characters
        if (BTserial.available() < 3) {
            Serial.println("Second Ack message sensed, waiting for full message");

            return;
        }

        currentMessage = BTserial.readStringUntil('k');

    } else {  // Undefined character or incomplete message

        BTserial.read();

    }

    if (currentMessage.length() > 0) { // Process messages

        if (currentMessage[0] == 'C') { // Can message
            // Store values, then reply with an inital ACK, only run when sending can message from robot --> server
            storeCanInfo(currentMessage);
            sendInitialAck();
            Serial.println("Can message recieved, sending back inital ACK, storing values.");

        } else if (currentMessage[0] == 'A') { // Inital Ack message
            // Reply with a Second ACK, only run when sending can message from server --> robot
            sendSecondAck();
            serverListening = !serverListening;
            Serial.println("Inital ACK recieved, sending back second ACK, robotListening = " + String(serverListening));

        } else if (currentMessage[0] == 'K') { // Second Ack message
            // Conlcude request, change robot listening status, only run when sending can message from robot --> server
            serverListening = !serverListening;
            Serial.println("Second ACK recieved, serverListening = " + String(serverListening));

        }

        lastRecieved = currentMessage;
        currentMessage = "";
    }

}
bool Uno::replyToTest() {
    if (BTserial.peek() == requestTest) {    // Test message recieved
        BTserial.read();
        BTserial.write(replyTest);
        Serial.println("Test message received, responding");                   // Only used to print messages in monitor
        return true;
    }
    return false;
}

void Uno::sendInitialAck() { // Only sends acks for can messages, need to expand later
    BTserial.print("ACa");
    BTserial.flush();
}
void Uno::sendSecondAck() {
    BTserial.print("KCk");
    BTserial.flush();
}
void Uno::sendCan(int R, int G, int B) {
    String message = "C" + String(R) + String(G) + String(B) + "c";
    BTserial.print(message);
    BTserial.flush();
}
