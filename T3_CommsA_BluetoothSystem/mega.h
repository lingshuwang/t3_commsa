#pragma once

enum Status: unsigned char {
    NOTHING = 0,
    ORDER = 1,
    ORDERCOMPLETE =2,
};

class Mega {
public:
    Mega();
    bool establishConnectionToServer();
    bool testConection();
    void sendCan();
    void serialEvent1(); //Triggered when there is data in buffer and runs after loop runs
    bool replyToTest();
    void storeCanInfo(String canMessage);
    void sendInitalAck();
    void sendSecondAck();
    void sendCan(int R, int G, int B);
    unsigned char setStatus(unsigned char);
    unsigned char getStatus();
private:
    unsigned char status;

